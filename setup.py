import setuptools

setuptools.setup(name='nxframe',
                 version='0.0.1',
                 description='Frame data in different ways',
                 #url='',
                 author='nexustix',
                 author_email='[email protected]',
                 license='MIT',
                 packages=setuptools.find_packages(),
                 zip_safe=False)
