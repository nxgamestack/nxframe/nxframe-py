from nxwrap.nxwrap_message import NxwrapMessage
from nxwrap.nxwrap import nxwrap_wrap


class NxwrapMultiplexer():

    def __init__(self):
        self._buffer = b''

    def push(self, message:NxwrapMessage):
        #self._buffer += nxwrap_wrap_message(message)
        #self._buffer += bytes(message)
        self._buffer += nxwrap_wrap(message.source_id, message.kind, message.channel_id, message.data)

    def peek(self):
        return self._buffer

    def pop(self):
        r = self.peek()
        self._buffer = b''
        return r
