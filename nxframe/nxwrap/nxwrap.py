import util

"""
Goals:
- Debuggable
- Human Readable
- Human Writable
- Easy to parse

(<source name> <message kind> <channel ID> <DATA>)
(nxcat dat 1 the quick brown fox jumps over the lazy dog)
(x x 1 x)

<program name> for debugging the distributed system
<message kind> to know how to act upon the message
<channel ID> which channel this message belongs to
<DATA> actual data
"""


def nxwrap_wrap(source_id, kind, channel_id, data: bytes):
    bmsg = b''
    bmsg += b'('
    bmsg += bytes(source_id, 'UTF-8')
    bmsg += b' '
    bmsg += bytes(kind, 'UTF-8')
    bmsg += b' '
    bmsg += bytes(str(channel_id), 'UTF-8')
    bmsg += b' '
    bmsg += data.replace(b'(', b'((').replace(b')', b'))')
    bmsg += b')'
    bmsg += b' ' # to explicitly mark the ) as non-escaped
    return bmsg


def nxwrap_unwrap(octets: bytes, frame_start=None, frame_end=None):
    frame_start = frame_start or 0
    frame_end = frame_end or len(octets)-1
    frame = octets[frame_start+1:frame_end]
    segs = frame.split(b' ', 3)
    if len(segs) < 4:
        util.eprint("<!> WARN Invalid frame")
        return None
    source_id = str(segs[0], "UTF-8")
    kind = str(segs[1], "UTF-8")
    channel_id = int(str(segs[2], "UTF-8"))
    data = segs[3].replace(b'((', b'(').replace(b'))', b')')
    return (source_id, kind, channel_id, data)


def nxwrap_find_frame(octets: bytes):
    header_start = util.find_byte_esc(octets, b'(')
    header_end = -1

    if header_start > -1:
        header_end = util.find_byte_esc(octets, b')', start_index=header_start)
        if header_end > -1:
            return (header_start, header_end)
    return None


#def nxwrap_wrap_message(message:NxwrapMessage):
#    return nxwrap_pack(message.source_id, message.kind, message.channel_id, message.data)
#
#
#def nxwrap_unwrap_message(octets: bytes, frame_start=None, frame_end=None):
#    r = nxwrap_unpack(octets, frame_start, frame_end)
#    if r:
#        return NxwrapMessage(*r)
#    return None
